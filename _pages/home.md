---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#594777"
  overlay_image: "/assets/images/night.jpg"
  og_image: "/assets/images/lake_social.jpg"
excerpt: "Unconference, deep work, and rebooting." 
feature_row:
  - image_path: /assets/images/lake.jpg
    alt: "A lake and mountains."
    title: "2025 in Beatenberg, Switzerland"
    excerpt: "Save the Date: Apr 13–17, 2025. Want to sponsor? [Get in touch!](/contact)"
    url: "https://events.digital-research.academy/event/47/"
    btn_class: "btn--primary"
    btn_label: "2025 website" 
  - image_path: /assets/images/osr24-group-pic.jpg
    alt: "Group picture of the 2024 participants."
    title: "2024 in Schoorl, the Netherlands"
    excerpt: "A truly dutch edition, #osr24nl."
    url: "https://openscienceretreat.eu"
    btn_class: "btn--primary"
    btn_label: "2024 website"
  - image_path: /assets/images/osr23-group-pic.jpg
    alt: "Group picture of the 2023 participants."
    title: "2023 in Kochel am See, Germany"
    excerpt: "The first retreat, #OpenScienceRetreat23."
    url: "https://2023.open.science-retreat.org"
    btn_class: "btn--primary"
    btn_label: "2023 website"
---



Whether you are just getting started with Open Science or are already an open hero.  
Whether you are a researcher or provide research services.  
**If you are ready for a week of new ideas, fresh air and community: this retreat is for you!**
{: .text-center}


{% include feature_row %}



