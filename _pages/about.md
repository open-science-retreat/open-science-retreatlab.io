---
permalink: /about/
layout: splash
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/lake.jpg
author_profile: false
title: "About"
---

The Open Science Retreat is an event from Open Science enthusiast for Open Science enthusiasts.
The main goals are:

- Learning
- Getting stuff done
- Networking
- Reflecting
- Rebooting

All this can be done better in beautiful nature, which is why we love our 
retreats in the mountains, dunes, forest, near a lake, or the sea.

---


The event was originally initiated by [Dr. Heidi Seibold](https://heidiseibold.com/) 
and is now run by the [Digital Research Academy](https://digital-research.academy/) 
and friends. 

Want to organise an Open Science Retreat? [Get in touch!](/contact)

